import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static final AppPreferences _instance = AppPreferences._internal();
  late SharedPreferences _sharedPreferences;

  factory AppPreferences() {
    return _instance;
  }

  AppPreferences._internal();

  Future<void> initPreferences() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  Future<void> setMarketName(String marketName) async{
    await _sharedPreferences.setString('marketName', marketName);
  }

  String getMarketName() {
    return _sharedPreferences.getString('marketName') ?? '';
  }

  Future<void> setCategory(String category) async{
    await _sharedPreferences.setString('category', category);
  }

  String getCategory() {
    return _sharedPreferences.getString('category') ?? '';
  }

  Future<void> setAmountPaid(int amountPaid) async{
    await _sharedPreferences.setInt('amountPaid', amountPaid);
  }

  int getAmountPaid() {
    return _sharedPreferences.getInt('amountPaid') ?? 0;
  }

  Future<void> setTotalPrice(int total) async{
    await _sharedPreferences.setInt('total', total);
  }

  int getTotalPrice() {
    return _sharedPreferences.getInt('total') ?? 0;
  }

}
