import 'dart:convert';

class Products {
  String name = '';
  int price = 0;
  bool states = false;
  int counter = 1;

  Products({required this.name, required this.price, required this.states,this.counter=1});

  factory Products.fromJson(Map<String, dynamic> jsonData) {
    return Products(name: jsonData['name'], price: jsonData['price'], states: jsonData['states'], counter: jsonData['counter']);
  }

  static Map<String, dynamic> toMap(Products products) => {
    'name': products.name,
    'price': products.price,
    'states': products.states,
    'counter': products.counter,
  };

  static String encode(List<Products> products) => json.encode(
    products
        .map<Map<String, dynamic>>((product) => Products.toMap(product))
        .toList(),
  );

  static List<Products> decode(String product) =>
      (json.decode(product) as List<dynamic>)
          .map<Products>((item) => Products.fromJson(item))
          .toList();
}
