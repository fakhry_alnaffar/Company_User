class Order{
  String name = '';
  List<dynamic> marketName = <dynamic> [];
  String path = '';
  String email = '';
  String done = '';
  String created = DateTime.now().toString().split(' ')[0];

  Order();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['marketName'] = marketName;
    map['created'] = created;
    map['email'] = email;
    map['done'] = done;
    return map;
  }
}