class Product {
  late String path='';
  late String nameProduct='';
  late String nameCategory='';
  late String price='';
  late String categoryId='';


  Product();

  Product.forMap(Map<String, dynamic> map) {
    nameProduct = map['nameProduct'];
    nameCategory = map['nameCategory'];
    price = map['price'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['nameProduct'] = nameProduct;
    map['nameCategory'] = nameCategory;
    map['price'] = price;
    map['categoryId'] = categoryId;
    return map;
  }
}
