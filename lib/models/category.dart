class Category {
  late String path='';
  late String categoryName='';


  Category();

  Category.forMap(Map<String, dynamic> map) {
    categoryName = map['categoryName'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['categoryName'] = categoryName;
    return map;
  }
}