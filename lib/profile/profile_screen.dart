import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_auth_controller.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/models/distributor.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/widgets/component.dart';
import 'package:graduation_flutter_yoth_user/widgets/lite_edit.dart';

class ProfileScreen extends StatefulWidget {

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String name = '';
  String id = '';
  String image = '';

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      endDrawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: 10,
      drawerEnableOpenDragGesture: true,
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(5),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'بيانات الحساب',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
        actions: [
          IconButton(onPressed: (){FbAuthController().signOut(context);}, icon: Icon(Icons.logout))
        ],
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: StreamBuilder<QuerySnapshot>(
              stream: FbFireStoreController().readUser(),
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  List<DocumentSnapshot> documents = snapshot.data!.docs;
                  return Container(
                    margin: EdgeInsets.only(top: 100),
                    width: 414,
                    height: double.infinity,
                    alignment: Alignment.bottomCenter,
                    decoration: BoxDecoration(
                      color: Color(0xffF0F4FD),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    child: Column(
                      children: [
                        SizedBox(height: 40),
                        DecoratedBox(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 0),
                                color: Colors.white.withOpacity(0.16),
                                blurRadius: 6,
                              ),
                            ],
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(80),
                              border: Border.all(width: 8, color: Colors.white),
                            ),
                            child: CircleAvatar(
                              maxRadius: 50,
                              minRadius: 50,
                              backgroundColor: Color(0xff5A55CA),
                              backgroundImage: NetworkImage(
                                  documents[0].get('image')),
                            ),
                          ),
                        ),
                        SizedBox(height: 14),
                        Text(
                          '${documents[0].get('name')}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff0B204C),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          '${documents[0].get('email')}',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Color(0xffb2bac9),
                          ),
                        ),
                        SizedBox(height: 15,),
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(20),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white),
                                child: Padding(
                                  padding: EdgeInsets.only(left: 15, top: 15),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            'Personal information',
                                            style: TextStyle(
                                                fontSize: 17, fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      ListEdit(
                                        onTap: () {},
                                        icon: Icons.format_list_numbered,
                                        title: '${documents[0].get('carNumber')}'
                                      ),
                                      Divider(
                                        indent: 20,
                                        endIndent: 20,
                                      ),
                                      ListEdit(
                                        onTap: () {},
                                        icon: Icons.car_rental,
                                        title: '${documents[0].get('typeCar')}'
                                      ),
                                      Divider(
                                        indent: 20,
                                        endIndent: 20,
                                      ),
                                      ListEdit(
                                        onTap: () {},
                                        icon: Icons.phone,
                                        title: '${documents[0].get('mobileNumber')}'
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                SizedBox(
                                  width: 25,
                                ),
                                Icon(
                                  Icons.settings,
                                  color: Color(0xff5A55CA),
                                  size: 25,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  ' Setting ',
                                  style: TextStyle(
                                    fontSize: 17,
                                    color: Color(0xff0B204C),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 15,),
                            InkWell(
                              child: showItemProfile(
                                  title: 'Change Password',
                                  subtitle: 'You Can Edit Password Account',
                                  icon: Icons.lock_outline_rounded),
                              onTap: () {
                                Navigator.pushNamed(context, 'change_password');
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                }else{
                  return Container();
                }

              }
            ),
          ),
        ],
      ),
    );
  }

  // Future<void> getUserName() async {
  //   String namee = await FbFireStoreController().getAdminName();
  //   setState(() {
  //     name = namee;
  //     print('Name ' + name);
  //   });
  // }
  //
  // Future<void> getAdminId() async{
  //   String ids = await FbFireStoreController().getAdminId();
  //   setState(() {
  //     id = ids;
  //   });
  // }

  Distributor getUser(DocumentSnapshot snapshot) {
    Distributor distributor = Distributor();
    distributor.email = snapshot.get('email');
    distributor.name = snapshot.get('name');
    distributor.image = snapshot.get('image');
    distributor.path = snapshot.id;
    distributor.carNumber = snapshot.get('carNumber');
    distributor.typeCar = snapshot.get('typeCar');
    distributor.mobileNumber = snapshot.get('mobileNumber');
    return distributor;
  }
}
