import 'package:flutter/material.dart';

class OrderItemList extends StatelessWidget {
  String name = '';
  int qunatity = 0;
  int price = 0;
  int total = 0;

  OrderItemList(this.name, this.qunatity, this.price, this.total);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(5.0),
          topRight: const Radius.circular(5.0),
          bottomRight: const Radius.circular(5.0),
          bottomLeft: const Radius.circular(5.0),
        ),
        gradient: LinearGradient(
          begin: AlignmentDirectional.topStart,
          end: AlignmentDirectional.bottomEnd,
          colors: [
            Color(0xff5A55CA),
            Color(0xcc5a55ca),
          ],
        ),
      ),
      width: double.infinity,
      margin: EdgeInsetsDirectional.only(start: 2, end: 2),
      height: 50,
      child: Row(
        children: [
          SizedBox(
            width: 20,
          ),
          Text(
            name,
            style: TextStyle(color: Colors.white),
          ),
          Spacer(),
          Spacer(),
          Text(
            qunatity.toString(),
            style: TextStyle(color: Colors.white),
          ),
          Spacer(),
          Text(
            price.toString(),
            style: TextStyle(color: Colors.white),
          ),
          Spacer(),
          Text(
            total.toString(),
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }
}

