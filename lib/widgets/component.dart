import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';

Widget homeWidget({
  required var icon,
  required String tittle,
  required String counterAndType,
  required Color iconColor,
  bool iconVisible = false,
  bool titleVisible = true,
}) {
  return Stack(
    children: [
      Container(
        width: SizeConfig().scaleWidth(171),
        height: SizeConfig().scaleHeight(171),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(14),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.16),
              spreadRadius: 0,
              blurRadius: 4,
              offset: Offset(0, 4), // changes position of shadow
            ),
          ],
        ),
      ),
      Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: SizeConfig().scaleHeight(59),
              width: SizeConfig().scaleWidth(59),
              child: Icon(
                icon,
                color: iconColor,
                size: SizeConfig().scaleWidth(40),
              ),
              decoration: BoxDecoration(
                color: Color(0xFFF0F4FD),
                borderRadius: BorderRadius.circular(14),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(16),
            ),
            Text(
              tittle,
              style: TextStyle(
                fontSize: SizeConfig().scaleTextFont(16),
                fontWeight: FontWeight.w500,
                color: Color(0xff0B204C),
              ),
            ),
          ],
        ),
      )
    ],
  );
}

Widget showItemProfile({
  required String title,
  required String subtitle,
  required IconData icon,
}) {
  return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 3.0, bottom: 3),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 0),
                child: CircleAvatar(
                  radius: 45,
                  child: Icon(
                    icon,
                    color: Colors.white,
                  ),
                  backgroundColor: Color(0xff5A55CA),
                ),
              ),
              SizedBox(
                width: 0,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  textBaseline: TextBaseline.ideographic,
                  children: [
                    Text(
                      title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      subtitle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Color(0xff919BB3),
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 30,
              ),
            ],
          ),
        ),
      ));
}

Widget showItem({
  required List<DocumentSnapshot> documents,
  required int index,
  required BuildContext context,
  required String title,
  required String subtitle,
  String? url,
}) {
  return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 3.0, bottom: 3),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 0),
                child: CircleAvatar(
                  backgroundImage: url != null ? NetworkImage(url) : null,
                  radius: 45,
                  child: url == null ? Icon(Icons.person) : null,
                  backgroundColor: Color(0xff5A55CA),
                ),
              ),
              SizedBox(
                width: 0,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  textBaseline: TextBaseline.ideographic,
                  children: [
                    Text(
                      title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      subtitle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Color(0xff919BB3),
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 30,
              ),
            ],
          ),
        ),
      ));
}

Widget showWidgetMeet(
    {required String status,
    required String title,
    required String description,
    required String time,
    required String date}) {
  return Card(
    clipBehavior: Clip.antiAlias,
    elevation: 0,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
              top: SizeConfig().scaleHeight(15),
              left: SizeConfig().scaleWidth(15)),
          child: Text(
            status,
            style: TextStyle(
                fontSize: SizeConfig().scaleTextFont(16),
                color: Color(0xff6e59e5)),
          ),
        ),
        Divider(
          indent: SizeConfig().scaleWidth(15),
          endIndent: SizeConfig().scaleWidth(15),
          color: Colors.grey,
        ),
        SizedBox(
          height: SizeConfig().scaleWidth(10),
        ),
        Row(
          children: [
            SizedBox(
              width: SizeConfig().scaleWidth(15),
            ),
            Container(
              width: SizeConfig().scaleWidth(3),
              height: SizeConfig().scaleHeight(50),
              color: Color(0xff6e59e5),
            ),
            SizedBox(
              width: SizeConfig().scaleWidth(15),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: SizeConfig().scaleTextFont(18),
                    fontWeight: FontWeight.bold,
                    color: Color(0xff0B204C),
                  ),
                ),
                SizedBox(
                  height: SizeConfig().scaleHeight(5),
                ),
                Text(
                  description,
                  style: TextStyle(
                      fontSize: SizeConfig().scaleTextFont(15),
                      color: Color(0xffB2BAC9)),
                ),
              ],
            ),
            Spacer(),
            IconButton(
              icon: Icon(Icons.delete_rounded,
                  size: SizeConfig().scaleHeight(26),
                  color: Colors.red.shade600),
              onPressed: () {
                //DELETE
                print('DELETE');
              },
            ),
            SizedBox(
              width: SizeConfig().scaleWidth(10),
            ),
          ],
        ),
        SizedBox(
          height: SizeConfig().scaleHeight(15),
        ),
        Row(
          children: [
            SizedBox(
              width: SizeConfig().scaleWidth(15),
            ),
            Row(
              children: [
                Icon(
                  Icons.timer,
                  color: Colors.grey,
                  size: SizeConfig().scaleHeight(20),
                ),
                SizedBox(
                  width: SizeConfig().scaleHeight(10),
                ),
                Text(
                  time,
                  style: TextStyle(
                      color: Color(0xff0B204C),
                      fontSize: SizeConfig().scaleTextFont(14)),
                )
              ],
            ),
            Spacer(),
            Row(
              children: [
                Icon(
                  Icons.date_range_rounded,
                  color: Colors.grey,
                  size: SizeConfig().scaleHeight(20),
                ),
                SizedBox(
                  width: SizeConfig().scaleWidth(10),
                ),
                Text(
                  date,
                  style: TextStyle(
                      color: Color(0xff0B204C),
                      fontSize: SizeConfig().scaleTextFont(14)),
                ),
                SizedBox(
                  width: SizeConfig().scaleWidth(15),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: SizeConfig().scaleHeight(20),
        ),
      ],
    ),
  );
}
