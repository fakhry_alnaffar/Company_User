import 'package:flutter/material.dart';

class ListEdit extends StatelessWidget {
  final IconData icon;
  final String title;
  void Function() onTap;

  ListEdit({
    required this.icon,
    required this.title,
    required this.onTap
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      leading: Icon(
        icon,
        color: Colors.purple,
        size: 23,
      ),
      title: Text(title)
    );
  }
}