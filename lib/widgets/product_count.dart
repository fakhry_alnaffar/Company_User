import 'package:flutter/material.dart';

class ProductCount extends StatelessWidget {
  final IconData icon;

  ProductCount({required this.icon});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(3),
        child: Icon(
          icon,
          color: Colors.white,
        ),
      ),
      color: Color(0xff5A55CA),
      elevation: 0,
    );
  }
}