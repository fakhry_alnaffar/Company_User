import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_auth_controller.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/utils/helpers.dart';
import 'package:graduation_flutter_yoth_user/widgets/app_text_filed.dart';

class ForgetPasswordScreen extends StatefulWidget {
  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen>
    with Helpers {
  late TextEditingController _emailTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _emailTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _emailTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(icon: Icon(Icons.arrow_back_ios_rounded), onPressed: () {
              Navigator.pop(context);
            },)),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'FORGET PASSWORD',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(100),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Align(
                          alignment: AlignmentDirectional.topStart,
                          child: Padding(
                            padding: const EdgeInsetsDirectional.only(
                                start: 10, top: 10, bottom: 0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Forget Password',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 22,
                                    color: Colors.black,
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(5),
                                ),
                                Text(
                                  'Enter Email...',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16,
                                      color: Colors.grey.shade500),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        AppTextFiled(
                          labelText: 'Email',
                          controller: _emailTextController,
                          textInputType: TextInputType.emailAddress,
                          maxLength: 30,
                          prefix: Icons.email,
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(20),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(double.infinity,
                                SizeConfig().scaleHeight(60)),
                            primary: Color(0xff5A55CA),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () {
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(
                                    builder: (context) => ForgetPasswordScreen()));
                          },
                          child: Text(
                            'SEND',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(18)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );  }

  Future<void> performForgetPassword() async {
    if (checkData()) {
      await forgetPassword();
    } else {}
  }

  bool checkData() {
    if (_emailTextController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, content: 'Enter Required Data', error: true);
    return false;
  }

  Future<void> forgetPassword() async {
    bool status = await FbAuthController()
        .forgetPassword(context, email: _emailTextController.text);
    if (status) {
      showSnackBar(
          context: context,
          content: 'Reset Email Sent Successfully, Check and Confirm');
      Navigator.pop(context);
    }
  }
}
