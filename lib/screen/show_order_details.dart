import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/models/order.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';

class ShowOrderDetails extends StatefulWidget {
  Order order;

  ShowOrderDetails({required this.order});

  @override
  _ShowOrderDetailsState createState() => _ShowOrderDetailsState();
}

class _ShowOrderDetailsState extends State<ShowOrderDetails> {
  List<Order> orders = <Order>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for(int i = 0; i < widget.order.marketName.length; i++){
      orders.add(widget.order.marketName[i]);
    }
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'تفاضيل المهمة',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
        actions: [
          IconButton(onPressed: (){deleteOrder();}, icon: Icon(Icons.done, color: Colors.white,))
        ],
      ),
      body: Stack(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: 100),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Name :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //name
                              widget.order.name,
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Date :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //date
                              widget.order.created,
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Expanded(
                        child: ListView.separated(
                          itemCount: orders.length,
                          itemBuilder: (context, index) {
                            return Text('');
                          },
                          padding: EdgeInsetsDirectional.only(top: 5),
                          separatorBuilder: (BuildContext context, int index) { return SizedBox(height: 5,); },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> deleteOrder() async{
    bool state = await FbFireStoreController().setOrderAsCompleted(path: widget.order.path);
    if(state){
      Navigator.pop(context);
    }
  }

}
