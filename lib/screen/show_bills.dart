import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/models/bills.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/widgets/component.dart';

import 'bills/show_bills_details.dart';

class BillsShow extends StatefulWidget {
  const BillsShow({Key? key}) : super(key: key);

  @override
  _BillsShowState createState() => _BillsShowState();
}

class _BillsShowState extends State<BillsShow> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FbFireStoreController().readBill();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'الفواتير',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: StreamBuilder<QuerySnapshot>(
                  stream: FbFireStoreController().readBill(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      List<DocumentSnapshot> documents = snapshot.data!.docs;
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 0),
                        child: ListView.separated(
                          shrinkWrap: false,
                          physics: BouncingScrollPhysics(),
                          clipBehavior: Clip.antiAlias,
                          padding: EdgeInsetsDirectional.only(top: 50),
                          itemBuilder: (context, index) {
                            return InkWell(
                              child: showItem(
                                  documents: documents,
                                  index: index,
                                  context: context,
                                  title: documents[index].get('marketName'),
                                  subtitle: documents[index].get('created')),
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ShowBillsDetails(getBill(documents[index])),));
                              },
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: SizeConfig().scaleHeight(14),
                            );
                          },
                          itemCount: documents.length,
                        ),
                      );
                    } else {
                      return Center(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.warning_amber_outlined,
                                color: Colors.grey,
                                size: 50,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'NO BILLS',
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 16,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  }),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushReplacementNamed(context, 'bill_add_num1');
        },
        child: Icon(Icons.add),
        backgroundColor: Color(0xff5A55CA),
      ),
    );
  }

  Bill getBill(DocumentSnapshot snapshot){
    Bill bill = Bill();
    bill.marketName = snapshot.get('marketName');
    bill.remainingAmount = snapshot.get('remainingAmount');
    bill.totalPrice = snapshot.get('totalPrice');
    bill.created = snapshot.get('created');
    bill.amountPaid = snapshot.get('amountPaid');
    bill.product = snapshot.get('product');
    bill.path = snapshot.id;
    return bill;
  }

}
//onDELETE
// FbFireStoreController().deleteBill(path: documents[index].id)
