import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/models/category.dart';
import 'package:graduation_flutter_yoth_user/models/product.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/utils/helpers.dart';
import 'package:graduation_flutter_yoth_user/widgets/component.dart';

class UserShowProductCategory extends StatefulWidget {
  Category category;

  UserShowProductCategory(this.category);

  @override
  _UserShowProductCategoryState createState() => _UserShowProductCategoryState();
}

class _UserShowProductCategoryState extends State<UserShowProductCategory>  with Helpers{
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'الاصناف',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: SizeConfig().scaleHeight(100)),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 24, vertical: 0),
                  child: StreamBuilder<QuerySnapshot>(
                      stream: FbFireStoreController().readCustomProduct(widget.category.path),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else if (snapshot.hasData &&
                            snapshot.data!.docs.isNotEmpty) {
                          List<QueryDocumentSnapshot> data =
                              snapshot.data!.docs;
                          return ListView.separated(
                            padding: EdgeInsetsDirectional.only(top: 50),
                            itemBuilder: (context, index) {
                              return InkWell(
                                child: showItem(
                                    documents: data,
                                    index: index,
                                    context: context,
                                    title: data[index].get('nameProduct'),
                                    subtitle: data[index].get('price').toString()),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return SizedBox(
                                height: SizeConfig().scaleHeight(14),
                              );
                            },
                            itemCount: data.length,

                            physics: BouncingScrollPhysics(),
                            shrinkWrap: false,
                            clipBehavior: Clip.antiAlias,
                          );
                        }else {
                          return Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(
                                  Icons.warning,
                                  size: 80,
                                  color: Colors.grey.shade500,
                                ),
                                Text(
                                  'No Data',
                                  style: TextStyle(
                                      color: Colors.grey.shade500,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ],
                            ),
                          );
                        }
                      })),
            ),
          ),
        ],
      ),
    );
  }
}
