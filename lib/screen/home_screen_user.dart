import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_auth_controller.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/models/dashboard_items.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/widgets/component.dart';

class HomeScreenUser extends StatefulWidget {
  const HomeScreenUser({Key? key}) : super(key: key);

  @override
  _HomeScreenUserState createState() => _HomeScreenUserState();
}

class _HomeScreenUserState extends State<HomeScreenUser> {

  String name = '';
  List<DashboardItem> items = <DashboardItem>[
    DashboardItem(
        title: 'الزبائن',
        counterAndType: '10 زبون',
        icon: Icons.person,
        iconColor: Color(0xff6e59e5)),
    DashboardItem(
        title: 'البضائع',
        counterAndType: ' البضائع',
        icon: Icons.list_outlined,
        iconColor: Color(0xffF26950)),
    DashboardItem(
        title: 'المهام المطلوبة',
        counterAndType: '18 مهمة ',
        icon: Icons.assignment,
        iconColor: Color(0xfff5c005)),
    DashboardItem(
        title: 'الفواتير ',
        counterAndType: '18 مهمة ',
        icon: Icons.request_page,
        iconColor: Color(0xff61bf21)),
    DashboardItem(
        title: 'الحساب',
        counterAndType: '18 مهمة ',
        icon: Icons.settings,
        iconColor: Color(0xff61bf21)),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserName();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      endDrawerEnableOpenDragGesture: true,
      drawerEdgeDragWidth: 10,
      drawerEnableOpenDragGesture: true,
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(5),
              bottom: SizeConfig().scaleWidth(0),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'شاشة المستخدم',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      // drawer: ClipRRect(
      //   borderRadius: BorderRadius.only(
      //       bottomRight: Radius.circular(150.0),
      //       topRight: Radius.circular(150)),
      //   child: Container(
      //     color: Color(0xff5A55CA),
      //     width: 210,
      //     child: Drawer(
      //       child: Container(
      //         decoration: BoxDecoration(
      //           gradient: LinearGradient(
      //             begin: AlignmentDirectional.topStart,
      //             end: AlignmentDirectional.bottomEnd,
      //             colors: [
      //               Color(0xff5A55CA),
      //               Color(0xff5A55CA),
      //               Color(0x995a55ca),
      //             ],
      //           ),
      //         ),
      //         child: ListView(
      //           clipBehavior: Clip.antiAliasWithSaveLayer,
      //           physics: NeverScrollableScrollPhysics(),
      //           padding: EdgeInsets.symmetric(vertical: 10),
      //           primary: true,
      //           children: [
      //             UserAccountsDrawerHeader(
      //               decoration: BoxDecoration(
      //                 borderRadius: BorderRadius.circular(0),
      //                 gradient: LinearGradient(
      //                   begin: AlignmentDirectional.topStart,
      //                   end: AlignmentDirectional.bottomEnd,
      //                   colors: [
      //                     Color(0xff5A55CA),
      //                     Color(0xff5A55CA),
      //                     Color(0x995a55ca),
      //                   ],
      //                 ),
      //               ),
      //               accountName: Text(name),
      //               accountEmail: Text('${FbAuthController().user.email}'),
      //               currentAccountPicture: CircleAvatar(
      //                 backgroundColor: Colors.white,
      //                 child: Icon(
      //                   Icons.person_outline_rounded,
      //                   size: 40,
      //                   color: Color(0xff5A55CA),
      //                 ),
      //               ),
      //               arrowColor: Colors.black,
      //             ),
      //             Divider(
      //               color: Colors.grey,
      //               endIndent: 5,
      //               indent: 0,
      //               thickness: 1.5,
      //             ),
      //             SizedBox(
      //               height: 5,
      //             ),
      //             ListTile(
      //               // isThreeLine: true,
      //               dense: true,
      //               selected: false,
      //               leading: Icon(
      //                 Icons.settings_applications,
      //                 size: 30,
      //                 color: Colors.white,
      //               ),
      //               title: Container(
      //                   margin: EdgeInsetsDirectional.only(bottom: 0),
      //                   padding: EdgeInsetsDirectional.only(bottom: 0),
      //                   child: Text(
      //                     'الاعدادات',
      //                     style: TextStyle(fontSize: 18, color: Colors.white),
      //                   )),
      //               onTap: () {
      //                 Navigator.pop(context);
      //                 Navigator.pushNamed(context, 'setting_screen');
      //               },
      //             ),
      //             SizedBox(height: SizeConfig().scaleHeight(10)),
      //             ListTile(
      //               dense: true,
      //               selected: false,
      //               leading: Icon(
      //                 Icons.login_outlined,
      //                 size: 30,
      //                 color: Colors.white,
      //               ),
      //               title: Container(
      //                   margin: EdgeInsetsDirectional.only(bottom: 0),
      //                   child: Text(
      //                     'Logout',
      //                     style: TextStyle(fontSize: 18, color: Colors.white),
      //                   )),
      //               onTap: () {
      //                 FbAuthController().signOut(context);
      //               },
      //             ),
      //           ],
      //         ),
      //       ),
      //     ),
      //   ),
      // ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: GridView.builder(
                    padding: EdgeInsetsDirectional.only(top: 50),
                    physics: BouncingScrollPhysics(),
                    clipBehavior: Clip.antiAlias,
                    itemCount: items.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: SizeConfig().scaleHeight(24),
                        crossAxisSpacing: SizeConfig().scaleHeight(24),
                        childAspectRatio: 171 / 171,
                        mainAxisExtent: 171),
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          if (index == 0) {
                            Navigator.pushNamed(context, 'user_customer_screen');
                          } else if (index == 1) {
                            Navigator.pushNamed(context, 'user_product_home_screen');
                          } else if (index == 2) {
                            Navigator.pushNamed(context, 'assignment_screen');
                          } else if (index == 3) {
                            Navigator.pushNamed(context, 'bills_show');
                          } else if (index == 4) {
                            Navigator.pushNamed(context, 'profile_screen');
                          }
                        },
                        child: homeWidget(
                          icon: items[index].icon,
                          counterAndType: items[index].counterAndType,
                          tittle: items[index].title,
                          iconColor: items[index].iconColor,
                        ),
                      );
                    }),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Future<void> getUserName() async{
    String namee = await FbFireStoreController().getName();
    setState(() {
      name = namee;
    });
  }
}
