import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/models/customers.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/widgets/component.dart';

class UserCustomerScreen extends StatefulWidget {
   const UserCustomerScreen({Key? key}) : super(key: key);

   @override
   _UserCustomerScreenState createState() => _UserCustomerScreenState();
}

class _UserCustomerScreenState extends State<UserCustomerScreen> {
   @override
   Widget build(BuildContext context) {
      SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
      return Scaffold(
         extendBodyBehindAppBar: true,
         appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            backgroundColor: Colors.transparent,
            title: Container(
               margin: EdgeInsetsDirectional.only(
                   top: SizeConfig().scaleWidth(20),
                   bottom: SizeConfig().scaleWidth(20),
                   end: SizeConfig().scaleWidth(0)),
               child: Text(
                  'الزبائن',
                  style: TextStyle(
                      letterSpacing: 2,
                      wordSpacing: 0.5,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig().scaleWidth(24)),
               ),
            ),
         ),
         body: Stack(
            children: [
               Container(
                  clipBehavior: Clip.antiAlias,
                  width: double.infinity,
                  height: double.infinity,
                  decoration: BoxDecoration(
                     gradient: LinearGradient(
                        begin: AlignmentDirectional.topStart,
                        end: AlignmentDirectional.bottomEnd,
                        colors: [
                           Color(0xff5A55CA),
                           Colors.white,
                        ],
                     ),
                  ),
               ),
               SizedBox(
                  height: SizeConfig().scaleHeight(140),
               ),
               Align(
                  child: Container(
                     clipBehavior: Clip.antiAlias,
                     margin: EdgeInsets.only(top: 100),
                     width: 414,
                     height: double.infinity,
                     alignment: Alignment.bottomCenter,
                     decoration: BoxDecoration(
                        color: Color(0xffF0F4FD),
                        borderRadius: BorderRadius.only(
                           topLeft: Radius.circular(40),
                           topRight: Radius.circular(40),
                        ),
                     ),
                     child: StreamBuilder<QuerySnapshot>(
                         stream: FbFireStoreController().readCustomer(),
                         builder: (context, snapshot) {
                            if(snapshot.hasData){
                               List<DocumentSnapshot> documents = snapshot.data!.docs;
                               return Padding(
                                  padding:
                                  const EdgeInsets.symmetric(horizontal: 24, vertical: 0),
                                  child: ListView.separated(
                                     padding: EdgeInsetsDirectional.only(top: 50),
                                     itemBuilder: (context, index) {
                                        return InkWell(
                                           child: showItem(
                                               documents: documents,
                                               index: index,
                                               context: context,
                                               title: documents[index].get('name'),
                                               subtitle: documents[index].get('address').toString()),
                                        );
                                     },
                                     itemCount: documents.length, separatorBuilder: (BuildContext context, int index) { return SizedBox(height: 8,); },
                                  ),
                               );
                            }else{
                               return Center(
                                  child: Container(
                                     child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                           Icon(
                                              Icons.warning_amber_outlined,
                                              color: Colors.grey,
                                              size: 50,
                                           ),
                                           SizedBox(
                                              height: 10,
                                           ),
                                           Text(
                                              'No items found',
                                              style: TextStyle(
                                                 color: Colors.grey,
                                                 fontSize: 16,
                                              ),
                                           ),
                                        ],
                                     ),
                                  ),
                               );
                            }
                         }
                     ),
                  ),
               ),
            ],
         ),
      );
   }

}
