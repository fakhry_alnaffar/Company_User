import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/models/products.dart';
import 'package:graduation_flutter_yoth_user/preferences/app_preferences.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/utils/helpers.dart';
import 'package:graduation_flutter_yoth_user/widgets/product_count.dart';

class AddBillNum2 extends StatefulWidget {
  const AddBillNum2({Key? key}) : super(key: key);

  @override
  _AddOrderNum2State createState() => _AddOrderNum2State();
}

class _AddOrderNum2State extends State<AddBillNum2> with Helpers {
  List<Products> products = <Products>[];
  List<Products> productsSelected = <Products>[];

  @override
  void initState() {
    // TODO: implement initState
    getProductName();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        actions: [
          Card(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleHeight(10),
                end: SizeConfig().scaleWidth(10)),
            color: Colors.white,
            child: TextButton(
              style: ButtonStyle(),
              onPressed: () {
                saveProduct();
              },
              child: Text('التالي',
                  style: TextStyle(
                      color: Color(0xff5A55CA),
                      fontSize: SizeConfig().scaleTextFont(18),
                      fontWeight: FontWeight.bold)),
            ),
          )
        ],
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'اضافة الاصناف',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: SizeConfig().scaleHeight(110)),
              width: SizeConfig().scaleWidth(414),
              height: double.infinity,
              alignment: Alignment.topCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(SizeConfig().scaleWidth(40)),
                  topRight: Radius.circular(SizeConfig().scaleWidth(40)),
                ),
              ),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 24, vertical: 0),
                child: ListView.separated(
                  padding: EdgeInsetsDirectional.only(top: 40),
                  itemBuilder: (context, index) {
                    return showProductList(index);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider(
                      thickness: 0.5,
                      height: 20,
                      color: Colors.grey.shade500,
                    );
                  },
                  itemCount: products.length,
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  clipBehavior: Clip.antiAlias,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> saveProduct() async {
    int total = 0;
    for (int i = 0; i < products.length; i++) {
      if (products[i].states) {
        productsSelected.add(products[i]);
        total += products[i].price * products[i].counter;
      }
    }
    if (productsSelected.length > 0) {
      String encodedData = Products.encode(productsSelected);
      await AppPreferences().setCategory(encodedData);
      await AppPreferences().setTotalPrice(total);
      print(AppPreferences().getCategory());
      print(AppPreferences().getTotalPrice());
      Navigator.pushReplacementNamed(context, 'order_num3');
    } else {
      showSnackBar(context: context, content: 'ادخل الاصناف', error: true);
    }
  }

  CheckboxListTile showProductList(int index) {
    return CheckboxListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        products[index].name,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
      subtitle: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '${products[index].price.toString()}',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            ' || ',
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: Colors.grey.shade500),
            textAlign: TextAlign.center,
          ),
          Text(
            '${products[index].price.toDouble() * products[index].counter}',
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: Colors.red.shade700),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
      checkColor: Colors.white,
      selectedTileColor: Colors.yellow,
      selected: products[index].states,
      tileColor: Colors.yellow,
      autofocus: false,
      dense: true,
      tristate: false,

      // secondary: ,
      activeColor: Color(0xff5A55CA),
      controlAffinity: ListTileControlAffinity.leading,
      secondary: SizedBox(
        width: 120,
        child: Row(
          children: [
            InkWell(
              child: ProductCount(
                icon: Icons.add,
              ),
              onTap: () {
                setState(() {
                  ++products[index].counter;
                });
              },
            ),
            SizedBox(width: 5),
            Text(
              products[index].counter.toString(),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(width: 5),
            InkWell(
              child: ProductCount(
                icon: Icons.minimize,
              ),
              onTap: () {
                setState(() {
                  setState(() {
                    if (products[index].counter > 1) {
                      --products[index].counter;
                    }
                  });
                });
              },
            ),
          ],
        ),
      ),
      isThreeLine: true,
      value: products[index].states,
      onChanged: (bool? status) {
        if (status != null)
          setState(() {
            products[index].states = status;
          });
      },
    );
  }

  Future<void> getProductName() async {
    List<String> names = await FbFireStoreController().getProductName();
    List<String> price = await FbFireStoreController().getProductPrice();
    for (int i = 0; i < names.length; i++) {
      print(names.length);
      setState(() {
        products.add(Products(
            name: names[i],
            states: false,
            price: int.parse(price[i]),
            counter: 1));
      });
    }
  }

}
