import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/models/bills.dart';
import 'package:graduation_flutter_yoth_user/models/products.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/widgets/order_item_list.dart';

class ShowBillsDetails extends StatefulWidget {
  Bill? bill;

  ShowBillsDetails(this.bill);

  @override
  _ShowBillsDetailsState createState() => _ShowBillsDetailsState();
}

class _ShowBillsDetailsState extends State<ShowBillsDetails> {
  List<Products> product = <Products>[];
  String products = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    products = widget.bill!.product;
    product = Products.decode(products);
  }
  
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'تفاضيل الفاتورة',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
        actions: [
          IconButton(onPressed: (){deleteBill();}, icon: Icon(Icons.done, color: Colors.white,))
        ],
      ),
      body: Stack(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: 100),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Market Name :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //name
                              widget.bill!.marketName,
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Date :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //date
                              widget.bill!.created,
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      itemTITLE(),
                      SizedBox(
                        height: 2,
                      ),
                      Expanded(
                        child: ListView.separated(
                          itemCount: product.length,
                          itemBuilder: (context, index) {
                            return OrderItemList(product[index].name, product[index].counter, product[index].price, product[index].counter * product[index].price);
                          },
                          padding: EdgeInsetsDirectional.only(top: 5),
                          separatorBuilder: (BuildContext context, int index) { return SizedBox(height: 5,); },
                        ),
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Total Price:',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //name
                              widget.bill!.totalPrice,
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Remaining :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //name
                              widget.bill!.remainingAmount,
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Amount Paid :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //name
                              widget.bill!.amountPaid,
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 40,)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget itemTITLE() {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(5.0),
          topRight: const Radius.circular(5.0),
          bottomRight: const Radius.circular(5.0),
          bottomLeft: const Radius.circular(5.0),
        ),
        gradient: LinearGradient(
          begin: AlignmentDirectional.topStart,
          end: AlignmentDirectional.bottomEnd,
          colors: [
            Color(0xff5A55CA),
            Color(0xd95a55ca),
          ],
        ),
      ),
      width: double.infinity,
      margin: EdgeInsetsDirectional.only(start: 2, end: 2),
      height: 50,
      child: Row(
        children: [
          SizedBox(
            width: 20,
          ),
          Text(
            'Item',
            style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 120,
          ),
          Text(
            'Quantity',
            style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
          ),
          Spacer(),
          SizedBox(
            width: 20,
          ),
          Text(
            'Price',
            style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
          ),
          Spacer(),
          Spacer(),
          Text(
            'Total',
            style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Future<void> deleteBill() async{
    bool state = await FbFireStoreController().setAsCompleted(path: widget.bill!.path);
    if(state){
      Navigator.pop(context);
    }
  }

}
