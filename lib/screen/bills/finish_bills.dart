import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';

class FinishBills extends StatefulWidget {
  const FinishBills({Key? key}) : super(key: key);

  @override
  _FinishBillsState createState() => _FinishBillsState();
}

class _FinishBillsState extends State<FinishBills> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      // String rout =FbAuthController().isLoggedIn?'main_screen':'login_screen';
      Navigator.pop(context);
    });
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFCE1E9FD),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              bottom: SizeConfig().scaleHeight(0)),
                          child: Icon(
                            Icons.check_circle_outline_rounded,
                            size: 80,
                            color: Colors.green,
                          ),
                        ),
                        Text(
                          'تم اضافة الفاتورة بنجاح',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: SizeConfig().scaleTextFont(30),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig().scaleHeight(20),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );  }
}
