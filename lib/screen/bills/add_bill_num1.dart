import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_firestore.dart';
import 'package:graduation_flutter_yoth_user/preferences/app_preferences.dart';
import 'package:graduation_flutter_yoth_user/responsive/size_config.dart';
import 'package:graduation_flutter_yoth_user/utils/helpers.dart';
import 'package:graduation_flutter_yoth_user/widgets/app_text_filed.dart';

class AddBillNum1 extends StatefulWidget {
  const AddBillNum1({Key? key}) : super(key: key);

  @override
  _AddBillNum1State createState() => _AddBillNum1State();
}

class _AddBillNum1State extends State<AddBillNum1> with Helpers {
  String? distributor;
  late TextEditingController _nameController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig().scaleWidth(0),
                bottom: SizeConfig().scaleWidth(0),
                start: SizeConfig().scaleWidth(5)),
            child: IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        backgroundColor: Colors.transparent,
        title: Container(
          margin: EdgeInsetsDirectional.only(
              top: SizeConfig().scaleWidth(20),
              bottom: SizeConfig().scaleWidth(20),
              end: SizeConfig().scaleWidth(0)),
          child: Text(
            'اضافة فاتورة',
            style: TextStyle(
                letterSpacing: 2,
                wordSpacing: 0.5,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig().scaleWidth(24)),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd,
                colors: [
                  Color(0xff5A55CA),
                  Colors.white,
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig().scaleHeight(140),
          ),
          Align(
            child: Container(
              margin: EdgeInsets.only(top: 100),
              width: 414,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF0F4FD),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    clipBehavior: Clip.antiAlias,
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: SizeConfig().scaleHeight(40),
                        ),
                        AppTextFiled(
                          labelText: 'اسم المحل',
                          controller: _nameController,
                          prefix: Icons.person,
                          readOnly: true,
                          showCursor: false,
                          onTap: () => showBottomSheet(),
                        ),
                        SizedBox(
                          height: SizeConfig().scaleHeight(30),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                double.infinity, SizeConfig().scaleHeight(60)),
                            primary: Color(0xff5A55CA),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onPressed: () async {
                            await saveMarketName();
                          },
                          child: Text(
                            'التالي',
                            style: TextStyle(
                                fontSize: SizeConfig().scaleTextFont(20)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> saveMarketName() async {
    if (_nameController.text.isNotEmpty) {
      await AppPreferences().setMarketName(_nameController.text);
      print(AppPreferences().getMarketName());
      Navigator.pushReplacementNamed(context, 'bill_add_num2');
    } else {
      showSnackBar(context: context, content: 'ادخل اسم السوبر ماركت', error: true);
    }
  }

  void showBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return StreamBuilder<QuerySnapshot>(
          stream: FbFireStoreController().readCustomer(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: ListView.builder(
                  itemCount: documents.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      title: Text(documents[index].get('marketName')),
                      value: documents[index].id,
                      groupValue: distributor,
                      onChanged: (String? value) {
                        if (value != null)
                          setState(
                            () {
                              distributor = value;
                              _nameController.text =
                                  documents[index].get('marketName');
                              Navigator.pop(context);
                            },
                          );
                      },
                    );
                  },
                ),
              );
            } else {
              return Container();
            }
          },
        );
      },
    );
  }
}
