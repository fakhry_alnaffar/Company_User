
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:graduation_flutter_yoth_user/firebase/fb_auth_controller.dart';
import 'package:graduation_flutter_yoth_user/models/bills.dart';
import 'package:graduation_flutter_yoth_user/models/category.dart';
import 'package:graduation_flutter_yoth_user/models/customers.dart';
import 'package:graduation_flutter_yoth_user/models/distributor.dart';
import 'package:graduation_flutter_yoth_user/models/order.dart';
import 'package:graduation_flutter_yoth_user/models/product.dart';

class FbFireStoreController {
  FirebaseFirestore _firebaseFireStore = FirebaseFirestore.instance;

  // *********************** User ************************* //

  Future<bool> addDistributor({required Distributor distributor}) async {
    return await _firebaseFireStore
        .collection('Distributor')
        .add(distributor.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateDistributor(
      {required String path, required Distributor distributor}) async {
    return await _firebaseFireStore
        .collection('Distributor')
        .doc(path)
        .update(distributor.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteDistributor({required String path}) async {
    return await _firebaseFireStore
        .collection('Distributor')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readDistributor() async* {
    yield* _firebaseFireStore.collection('Distributor').snapshots();
  }

  Future<bool> isUser() async {
    return _firebaseFireStore
        .collection('Distributor')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) {
      if (value.docs.length > 0) {
        return true;
      } else {
        return false;
      }
    });
  }

  Future<String> getUserName() async {
    return _firebaseFireStore
        .collection('Distributor')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('name'));
  }

  Future<String> getAdminName() async {
    return _firebaseFireStore
        .collection('Admin')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('name'));
  }

  Stream<QuerySnapshot> readUser() async* {
    yield* _firebaseFireStore.collection('Distributor').where('email', isEqualTo: FbAuthController().user.email).snapshots();
  }

  Future<bool> updateAdminImage(
      {required String path, required String image}) async {
    return await _firebaseFireStore
        .collection('Distributor')
        .doc(path)
        .update({'image' : image})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateAdminName(
      {required String path, required String name}) async {
    return await _firebaseFireStore
        .collection('Distributor')
        .doc(path)
        .update({'name' : name})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<String> getName() async {
    return _firebaseFireStore
        .collection('Distributor')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('name'));
  }

  Future<List<String>> getProductName() async {
    return await _firebaseFireStore.collection('product').get().then((value) {
      List<String> names = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        names.add(value.docs[i].get('nameProduct').toString());
      }
      return names;
    });
  }

  Future<List<String>> getProductPrice() async {
    return await _firebaseFireStore.collection('product').get().then((value) {
      List<String> names = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        names.add(value.docs[i].get('price').toString());
      }
      return names;
    });
  }

  Future<List<String>> getMarkets() async {
    return await _firebaseFireStore.collection('Order').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).get().then((value) => value.docs[0].get('marketName'));
  }

  // *********************** Customer ************************* //

  Future<bool> addCustomer({required Customers customer}) async {
    return await _firebaseFireStore
        .collection('Customer')
        .add(customer.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateCustomer(
      {required String path, required Customers customer}) async {
    return await _firebaseFireStore
        .collection('Customer')
        .doc(path)
        .update(customer.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteCustomer({required String path}) async {
    return await _firebaseFireStore
        .collection('Customer')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readCustomer() async* {
    yield* _firebaseFireStore.collection('Customer').snapshots();
  }

  Future<List<String>> getCustomerName() async {
    return await _firebaseFireStore.collection('Customer').get().then((value) {
      List<String> names = <String>[];
      for (int i = 0; i < value.docs.length; i++) {
        names.add(value.docs[i].get('name').toString());
      }
      return names;
    });
  }

  // *********************** Order Collection *********************** //

  Future<bool> addOrder({required Order order}) async {
    return await _firebaseFireStore
        .collection('Order')
        .add(order.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateOrder({required String path, required Order order}) async {
    return await _firebaseFireStore
        .collection('Order')
        .doc(path)
        .update(order.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteOrder({required String path}) async {
    return await _firebaseFireStore
        .collection('Order')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readOrder() async* {
    yield* _firebaseFireStore.collection('Order').snapshots();
  }

  Stream<QuerySnapshot> readUserOrder() async* {
    yield* _firebaseFireStore
        .collection('Order')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).where('done', isEqualTo: '0')
        .snapshots();
  }

  // *********************** bills Collection *********************** //

  Future<bool> addBill({required Bill bill}) async {
    return await _firebaseFireStore
        .collection('bill')
        .add(bill.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> setAsCompleted({required path}) async {
    return await _firebaseFireStore
        .collection('bill')
        .doc(path)
        .update({'done' : '1'})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> setOrderAsCompleted({required path}) async {
    return await _firebaseFireStore
        .collection('Order')
        .doc(path)
        .update({'done' : '1'})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteBill({required String path}) async {
    return await _firebaseFireStore
        .collection('bill')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readBill() async* {
    yield* _firebaseFireStore.collection('bill').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).where('done', isEqualTo: '0').snapshots();
  }

  // *********************** Counter Collection *********************** //

  Future<int> readCustomerCount() async {
    return await _firebaseFireStore.collection('Customer').get().then((value) {
      return value.docs.length;
    });
  }

  Future<int> readEmployeeCount() async {
    return await _firebaseFireStore
        .collection('Distributor')
        .get()
        .then((value) {
      return value.docs.length;
    });
  }

  Future<int> readOrderCount() async {
    return await _firebaseFireStore.collection('Order').get().then((value) {
      return value.docs.length;
    });
  }

  Future<int> readProductCount() async {
    return await _firebaseFireStore.collection('product').get().then((value) {
      return value.docs.length;
    });
  }

  Future<int> readBillsCount() async {
    return await _firebaseFireStore.collection('bill').get().then((value) {
      return value.docs.length;
    });
  }

  Stream<QuerySnapshot> readCategory() async* {
    yield* _firebaseFireStore
        .collection('category')
        .orderBy('categoryName', descending: true)
        .snapshots();
  }

  Stream<QuerySnapshot> readProduct() async* {
    yield* _firebaseFireStore
        .collection('product')
        .orderBy('nameProduct', descending: false)
        .snapshots();
  }

  Stream<QuerySnapshot> readCustomProduct(String category) async* {
    yield* _firebaseFireStore
        .collection('product')
        .where('categoryId', isEqualTo: category)
        .snapshots();
  }

  Future<bool> createCategory({required Category category}) async {
    return await _firebaseFireStore
        .collection('category')
        .add(category.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> createProduct({required Product product}) async {
    return await _firebaseFireStore
        .collection('product')
        .add(product.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateCategory(
      {required String path, required Category category}) async {
    return await _firebaseFireStore
        .collection('category')
        .doc(path)
        .update(category.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateProduct(
      {required String path, required Product product}) async {
    return await _firebaseFireStore
        .collection('product')
        .doc(path)
        .update(product.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteCategory({required String path}) async {
    return await _firebaseFireStore
        .collection('category')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteProduct({required String path}) async {
    return await _firebaseFireStore
        .collection('product')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

}
