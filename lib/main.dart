import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:graduation_flutter_yoth_user/preferences/app_preferences.dart';
import 'package:graduation_flutter_yoth_user/profile/profile_screen.dart';
import 'package:graduation_flutter_yoth_user/screen/auth/change_password.dart';
import 'package:graduation_flutter_yoth_user/screen/auth/foreget_password_screen.dart';
import 'package:graduation_flutter_yoth_user/screen/bills/add_bill_num1.dart';
import 'package:graduation_flutter_yoth_user/screen/bills/add_bill_num2.dart';
import 'package:graduation_flutter_yoth_user/screen/launch_screen.dart';
import 'package:graduation_flutter_yoth_user/screen/auth/login_screen.dart';
import 'package:graduation_flutter_yoth_user/screen/bills/finish_bills.dart';
import 'package:graduation_flutter_yoth_user/screen/bills/add__num3_order.dart';
import 'package:graduation_flutter_yoth_user/screen/bills/add__num4_order.dart';
import 'package:graduation_flutter_yoth_user/screen/show_bills.dart';
import 'package:graduation_flutter_yoth_user/screen/customer_screen.dart';
import 'package:graduation_flutter_yoth_user/screen/home_screen_user.dart';
import 'package:graduation_flutter_yoth_user/screen/product_home.dart';
import 'package:graduation_flutter_yoth_user/screen/show_assignment.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppPreferences().initPreferences();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute:'launch_screen' ,
      routes: {
        'launch_screen': (context) => LaunchScreen(),
        'login_screen': (context) => LoginScreen(),
        'forgot_password': (context) => ForgetPasswordScreen(),
        'user_customer_screen': (context) => UserCustomerScreen(),
        'user_product_home_screen': (context) => UserProductHomeScreen(),
        'order_num3': (context) => AddOrderNum3(),
        'order_num4': (context) => AddOrderNum4(),
        'bill_add_num1': (context) => AddBillNum1(),
        'bill_add_num2': (context) => AddBillNum2(),
        'home_screen_user': (context) => HomeScreenUser(),
        'assignment_screen': (context) => AssignmentScreen(),
        'bills_show': (context) => BillsShow(),
        'change_password': (context) => ChangePassword(),
        'finish_bills': (context) => FinishBills(),
        'profile_screen': (context) => ProfileScreen(),
      },
    );
  }
}
